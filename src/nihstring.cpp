#include "libnih.hpp"
#include <limits>

bool NIH::string::starts_with(std::string srchstr, std::string start) {
	if (srchstr.substr(0, start.length()) == start) {
		return true;
	}
	return false;
}

bool NIH::string::contains(std::vector<std::string> vec, std::string elem) {
	for (size_t i = 0; i < vec.size(); i++) {
		if (vec.at(i) == elem) {
			if (i <= std::numeric_limits<int>::max()) {
				return true;
			}
		}
	}
	return false;
}

bool NIH::string::contains(std::vector<std::string> vec, std::string elem, size_t& out_position) {
	for (size_t i = 0; i < vec.size(); i++) {
		if (vec.at(i) == elem) {
			if (i <= std::numeric_limits<int>::max()) {
				out_position = i;
				return true;
			}
		}
	}
	return false;
}

