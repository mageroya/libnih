#ifndef LIBNIH_HPP
#define LIBNIH_HPP

#include <string>
#include <vector>

namespace NIH {
	/* some basic string functions that are useful */
	namespace string {
		/* check if a string starts with a given string */
		bool starts_with(std::string srchstr, std::string start);

		/* check if a vector of strings contains a certain element */
		bool contains(std::vector<std::string> vec, std::string elem);
		bool contains(std::vector<std::string> vec, std::string elem, size_t& out_position);
	}

	/* basic directory and file handeling for linux */
	namespace dir {

	}
}

#endif
