LIBRARY = libnih.a
PACKFILE = libnih_src
SIGNING_IDENTITY = E128B5AE

SRCS := $(wildcard src/*.cpp)
HEADS := $(wildcard src/*.hpp) $(wildcard include/*.hpp) 
OBJS := $(addprefix obj/,$(notdir $(SRCS:.cpp=.o)))

CC = g++ -std=c++11 -m64
DEBUG = -g3 -O0
AFLAGS = -Wall -Wextra -Wconversion -Wsign-compare -Wshadow -Wzero-as-null-pointer-constant -pedantic $(DEBUG)
CFLAGS = -I include -c
LDFLAGS = #-pthread

all: dirs lib/$(LIBRARY)

lib/$(LIBRARY): $(OBJS)
	@echo "[\033[94mpacking\033[0m] "$@
	@ar r $@ $^	

obj/%.o: src/%.cpp $(HEADS)
	@echo "[\033[94mcompiling\033[0m] "$<"..."
	@$(CC) $(AFLAGS) $(CFLAGS) -o $@ $<

.PHONY: tidy clean re pack verify dirs init

init:
	@$(MAKE) --no-print-directory dirs
	@git init

dirs:
	@mkdir -p lib obj src include

re:
	@$(MAKE) --no-print-directory clean
	@$(MAKE) --no-print-directory all
	@$(MAKE) --no-print-directory pack
	@$(MAKE) --no-print-directory verify

tidy: 
	@echo "[\033[94mtidying project\033[0m]"
	@rm -rf obj
	@rm -f core

clean: tidy
	@echo "[\033[94mcleaning project\033[0m]"
	@rm -rf lib
	@rm -f *.gz *.sig

pack:
	@echo "[\033[94mpacking source\033[0m]"
	@tar cvf - include src stolencode makefile | pigz -p4 -9 > $(PACKFILE).tar.gz
	@echo "[\033[94msigning packfile\033[0m]"
	@gpg -u $(SIGNING_IDENTITY) --output $(PACKFILE).tar.gz.sig --detach-sig $(PACKFILE).tar.gz
	@tar cvf - *.tar.gz* | pigz -p4 -9 > $(PACKFILE).signed.tar.gz

verify:
	@echo "[\033[94mverifying signature\033[0m]"
	@gpg --verify $(PACKFILE).tar.gz.sig $(PACKFILE).tar.gz

